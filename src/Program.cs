﻿using exercises.Challenges.LinkedList;
using System;
using System.Linq;
using System.Threading;

namespace exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            var ex1 = new ReverseList();
            ex1.Fill("the sky is blue");
            ex1.Run();
            Console.ReadLine();
        }
    }
}
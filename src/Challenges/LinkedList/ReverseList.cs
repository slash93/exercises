﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/*
 * 
 * Reverse a linked list taking into account 
 * whole words separated by spaces
 * EX:
 *  input =>  [s]->[k]->[y]->[ ]->[i]->[s]->[ ]->[b]->[l]-[u]->[e]
 *  output => [b]->[l]-[u]->[e]->[ ]->[i]->[s]->[ ]->[s]->[k]->[y]
 *
 */

namespace exercises.Challenges.LinkedList
{
    public class Node
    {
        public char value;
        public Node next;
        public Node(char c)
        {
            value = c;
            next = null;
        }
    }

    public class LinkedList
    {
        Node head;

        public void Add(char value)
        {
            if(head == null)
            {
                head = new Node(value);
            }
            else
            {
                var l = Last();
                l.next = new Node(value);
            }
        }

        public Node Last()
        {
            Node iter = head;
            while(iter.next != null)
            {
                iter = iter.next;
            }
            return iter;
        }

        public void Reverse()
        {
            Node n = Reverse(head);

            Node iter = n;
            while (iter.next != null)
            {
                Console.Write(iter.value);
                iter = iter.next;
            }
            if (iter != null)
            {
                Console.Write(iter.value);
            }
        }

        private Node Reverse(Node cab)
        {
            Node iter = cab;
            Node prev = null;

            while (iter.next != null)
            {
                if(iter.value == ' ')
                {
                    if(prev == null)
                    {
                        iter.value = '\0';
                    }

                    Node next = iter.next;
                    iter.next = prev;
                    prev = cab;
                    cab = next;
                    iter = next;

                }
                else
                {
                    iter = iter.next;
                }
            }

            iter.next = new Node(' ');
            iter.next.next = prev;


            return cab;
        }

        public void Print()
        {
            Node iter = head;
            while (iter.next != null)
            {
                Console.Write(iter.value);
                iter = iter.next;
            }
            if (iter != null)
            {
                Console.Write(iter.value);
            }
        }
    }

    public class ReverseList
    {
        LinkedList list = new LinkedList();
        public void Fill(string str)
        {
            foreach(char c in str)
            {
                list.Add(c);
            }
        }

        public void Run()
        {
            list.Reverse();
        }

        public void Print()
        {
            list.Print();
        }
    }
}
